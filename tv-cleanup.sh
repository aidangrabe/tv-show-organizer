#!/bin/bash

D_SRC=~/Downloads
D_DST='/media/aidan/Mumbles-2.0/TV'

UNIMPORTANT_WORDS=(in the for and)

CLEAN_TORRENTS=true

# get the names of the tv shows to find
TV_SHOWS=("$(find "$D_DST" -maxdepth 1 -type d)")

# set the field separator to allow for spaces in file names
IFS_RESET=$IFS
IFS_ALLOW_SPACE=$(echo -en "\n\b")
IFS=$IFS_ALLOW_SPACE

for show in $TV_SHOWS; do
    [ $show == $D_DST ] && continue
    path=$show
    name=$(basename $show)
    name=$(echo $name | sed 's/,//g')
    words=("$(echo $name)")
    keywords=

    echo ==== $name ====

    # reset the field separator
    IFS=$IFS_RESET

    # get the keywords from the name of the folder
    for word in $words; do
        exists=$(printf "%s\n" ${UNIMPORTANT_WORDS[@]} | grep -i ^$word)
        [ ! -z $exists ] && continue
        keywords="${keywords} ${word}"
    done

    # build the grep argument to find the keywords in the src folder
    cmd=
    for keyword in $keywords; do
        cmd="${cmd}${keyword}.*"
    done

    # search the src folder for the keywords of this TV show
    episodes=("$(find $D_SRC | grep -i "${cmd}.[mp4|avi|mkv]$")")  #extensions

    IFS=$IFS_ALLOW_SPACE

    # go through each found episode, and move it to the correct location
    for episode in $episodes; do
        episode_name="$(basename $episode)"

        # regex example: SeriesNames01e05HDTV.mp4
        regex=".*[S|s]([0-9]{2})[E|e]([0-9]{2}).*"

        # regular expression to pull season + episode numbers from the filename
        [[ $episode =~ $regex ]] && {
            season=${BASH_REMATCH[1]}
            episode_number=${BASH_REMATCH[2]}
        }
    
        [ "${season:0:1}" == "0" ] && season=${season:1:2}
        #[ "${episode_number:0:1}" == "0" ] && episode_number=${episode_number:1:2}

        # move the episodes to the correct directory
        echo $episode_name
        dst_file="${path}/Season ${season}/${episode_number} - ${episode_name}"
        mkdir -p "${path}/Season ${season}"
        mv "${episode}" "${dst_file}"

        # rename file at new location using tvdb API
        tv-rename.sh "$name" "$dst_file"

    done
    
done

# cleanup the torrents
$CLEAN_TORRENTS && rm $D_SRC/*.torrent
