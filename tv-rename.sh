#!/bin/bash

APP_DIR=~/.tv-info
APP_SERIES_ID_FILE=${APP_DIR}/series-ids

# usage
#   usage
#   prints the usage of the script
function usage() {
    scriptname="$(basename $0)"
    echo "Usage $scriptname [series] [file|dir]"
    echo -e "\tseries - the name of the series which the file/dir belongs to"
    echo -e "\tfile|dir - the file or directory to rename. The file name must contain \
the string 's[0-99]e[0-99]'"
    exit 0
}

# print usage on miss-use (that's Mrs.Use to you!)
[ $# -lt 2 ] && usage

D="$(cd $(dirname $0) && pwd)"

# usage:
#   getEpisodeList [seriesID]
function getEpisodeList() {
    echo "Downloading episode list.."
    wget -O "${APP_DIR}/${1}.xml" "http://thetvdb.com/data/series/${1}/all/" &> /dev/null
}

# usage:
#   getSearchName [series-name]
function getSearchName() {
    echo "$1" | sed 's/ /%20/g'
}

# usage
#   getTag [search-tag] [xml-file]
function getTag() {
    echo $(grep "<${1}>" "$2" | sed "s/<[^>]\+>//g")
}

# usage
#   getSeriesId [safe-series-name]
#   safe-series-name        by safe I mean URL safe. Ie escape characters such
#                           as space to %20
function getSeriesId() {
    id=
    # try and get the series id from the cache
    if [ -e $APP_SERIES_ID_FILE ]; then
        id=$(grep "$1" "$APP_SERIES_ID_FILE" | sed "s/${1}=//g")
    else
        mkdir -p "$APP_DIR"
        touch "$APP_SERIES_ID_FILE"
    fi

    # if the id was not found in the ids file
    [ -z $id ] && {
        tmp='/tmp/series-id.xml'
        wget -O "$tmp" "http://thetvdb.com/api/GetSeries.php?seriesname=${1}" &> /dev/null
        id=$(getTag "id" "$tmp")
        echo "${1}=${id}" >> "$APP_SERIES_ID_FILE"
        rm $tmp
    }
    echo $id
}

# usage:
#   rename seriesName [fileName]
#   filename|               the filename/dirname to rename.
function rename() {
    file=$(basename "$1")
    dir=$(dirname "$1")
    [ -d "$1" ] && return

    season_num=
    episode_num=

    regex=".*[S|s]([0-9]{2})[E|e]([0-9]{2}).*"
    [[ "$file" =~ $regex ]] && {
        season_num=${BASH_REMATCH[1]}
        episode_num=${BASH_REMATCH[2]}
    }

    # if we couldn't find the pattern, then ignore the file
    # TODO: add warning message?
    if [ -z $season_num ] || [ -z $episode_num ]; then
        return
    fi

    # replace leading zeros.. easier way? probably.
    episode_num_leading_zero=$episode_num
    [ "${season_num:0:1}" == "0" ] && season_num=${season_num:1:2}
    [ "${episode_num:0:1}" == "0" ] && episode_num=${episode_num:1:2}

    extension="${file##*.}"

    # get the episode listing for the current series
    episodes_xml="${APP_DIR}/${series_id}.xml"
    if [ ! -e "${episodes_xml}" ]; then
        getEpisodeList $series_id
    fi

    # get the episode name
    episode_name=$(tv-ep-lookup.py "$episodes_xml" $season_num $episode_num)
    [ -z "$episode_name" ] && {
        getEpisodeList $series_id
        episode_name=$(tv-ep-lookup.py "$episodes_xml" $season_num $episode_num)
    }
    [ -z "$episode_name" ] && {
        quit "Could not find name for ${1}" 1
        return
    }

    # rename the file
    new_name="${episode_num_leading_zero} - ${episode_name}.${extension}"
    mv "$1" "${dir}/${new_name}"
    echo "$1 -> $new_name"
}

# get the series name
series=$(getSearchName "$1")
series_id=$(getSeriesId "$series")

if [ -d "$2" ]; then
    # directory
    d=("$(find "$2" -maxdepth 1)")

    IFS=$(echo -en "\n\b")
    for file in ${d[@]}; do
        rename $file
    done
else
    # single file
    rename "$2"
fi
