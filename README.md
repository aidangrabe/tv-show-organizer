# TV Show Organizer
A few scripts to clean up your downloads folder and move them into your TV Shows folder and rename each episode using [tvdb](http://www.thetvdb.com)'s API.

## Folder Structure
Your Tv Shows folder should have this structure

        /TV/Show/Parent/Directory/
        |--Series Name
        |----Season 1
        |------Episode 1
        |----Season 2
        |----Season N
        |--Another Series Name


## Usage
 - Cleanup Script
    Edit the script to set your SRC and DST folders

        ./tv-cleanup.sh

 - Rename Script
    Rename a file by looking it up for the given series

        ./tv-rename.sh [series] [file|dir]

 - Episode Lookup
    Lookup an episode name

        ./tv-ep-lookup.py [season-num] [episode-num]
