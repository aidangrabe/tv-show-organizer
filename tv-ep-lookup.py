#!/usr/bin/env python

import sys
import xml.dom.minidom as xml

def main(args):
    dom = xml.parse(args[0])
    
    episodes = dom.getElementsByTagName('Episode')

    for episode in episodes:
        if episode.getElementsByTagName('SeasonNumber').item(0).childNodes[0].data == args[1] and \
            episode.getElementsByTagName('EpisodeNumber').item(0).childNodes[0].data == args[2]:
            print episode.getElementsByTagName('EpisodeName').item(0).childNodes[0].data

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
